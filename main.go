package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/nlopes/slack"
	"gopkg.in/yaml.v2"
)

type Config struct {
	SlackToken string   `yaml:"slack_token"`
	Endpoints  []string `yaml:"endpoints"`
}

func main() {
	config, err := loadConfig("config.yaml")
	if err != nil {
		panic(err)
	}

	api := slack.New(config.SlackToken)

	for _, endpoint := range config.Endpoints {
		go monitorEndpoint(endpoint, api)
	}

	// Keep the main goroutine running indefinitely
	select {}
}

func monitorEndpoint(endpoint string, api *slack.Client) {
	for {
		// Send an HTTP GET request to the endpoint
		resp, err := http.Get(endpoint)
		if err != nil {
			sendSlackAlert(api, endpoint, err.Error())
		} else {
			resp.Body.Close()

			// If the response status code is not in the 200 range, consider it as an error
			if resp.StatusCode < 200 || resp.StatusCode >= 300 {
				sendSlackAlert(api, endpoint, fmt.Sprintf("HTTP status code: %d", resp.StatusCode))
			}
		}

		// Sleep for 1 minute before checking the endpoint again
		time.Sleep(time.Minute)
	}
}

func sendSlackAlert(api *slack.Client, endpoint string, message string) {
	channelID, _, err := api.PostMessage("#uptime-alerts", slack.MsgOptionText(fmt.Sprintf("Endpoint %s is down: %s", endpoint, message), false))
	if err != nil {
		fmt.Printf("Failed to send Slack alert: %s\n", err.Error())
	} else {
		fmt.Printf("Slack alert sent to channel %s\n", channelID)
	}
}

func loadConfig(filename string) (*Config, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var config Config
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}
