![Atumcell](https://atumcell.com/static/assets/img/logo-dark.png)

# Uptime Tracker

The Uptime Tracker is a Go-based application that monitors multiple domains and endpoints specified in a configuration file. It sends Slack alerts if any of the monitored endpoints are down.

## Getting Started

These instructions will help you get a copy of the project up and running on your local machine.

### Prerequisites

- Go (version 1.16 or higher) installed on your machine. You can download it from the official website: [https://golang.org/dl/](https://golang.org/dl/)

### Installation

1. Clone the repository:

```shell
git clone https://gitlab.com/atc-labs/uptime-monitor.git
```

2. Navigate to the project directory:

```shell
cd uptime-monitor
```

3. Configuration:
   - Create a file named `config.yaml` in the project directory.
   - Populate `config.yaml` with the desired configuration, following the provided example in the README file.
   - Make sure to replace `YOUR_SLACK_TOKEN` with your actual Slack API token.
   - Specify the endpoints you want to monitor in the `endpoints` section. Add or remove endpoints as needed.

4. Install dependencies:

```shell
go mod download
```

### Usage

1. Run the project:

```shell
go run main.go
```

   The program will start monitoring the specified endpoints and send Slack alerts if any of them are down.

2. Keeping the program running:
   - By default, the program uses `select{}` to keep the main goroutine running indefinitely, allowing continuous monitoring.
   - To stop the program, terminate it manually (e.g., by pressing Ctrl+C).

3. Error Handling and Customization:
   - The provided code is a basic example, and you can modify it based on your specific requirements.
   - Feel free to add error handling, logging, or any additional features you may need.

## Dependencies

- [nlopes/slack](https://github.com/nlopes/slack): A Go library for the Slack API.

## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvements, please open an issue or submit a merge request.

## License

This project is licensed under the [MIT License](https://gitlab.com/atc-labs/uptime-monitor/-/blob/main/LICENSE).
